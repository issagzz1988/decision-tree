import numpy as np 
from tree import Tree
from sklearn.model_selection import train_test_split

def learn(X, y, impurity_measure='entropy', pruning=False):
    """
    Make a decision tree with the 
    Args:
        X (numpy.ndarray): 
        y (numpy.ndarray): 
    Returns:
        A decision tree using id3
    Raises:
        ValueError: If the length of the columns of X are not the same length as y.
        ValueError: If impurity_measure is not a valid function (gini or entropy)
    """
    if X.T[0].size != len(y):
        raise ValueError("Columns of X must be of same length as length of y.")

    impurity_functions = {'entropy' : entropy, 'gini' : gini}
    if not impurity_measure in impurity_functions.keys():
        raise ValueError("No impurity_measure with name", impurity_measure, "found.") 

    if pruning:
        X_train, X_prune, y_train, y_prune = train_test_split(X, y, test_size=0.1, random_state=42)
        tree = make_tree(X_train, y_train, impurity_functions[impurity_measure])

        prune(X_prune, y_prune, tree)
        return tree

    return make_tree(X, y, impurity_functions[impurity_measure])

def make_tree(X, y, impurity_func):
    """
    Args:
        X (numpy.ndarray): Matrix with feature values
        y (list): The classifiers
        impurity_func (function): For calculating information gain.
    Returns:
        A decision tree
    """
    
    if is_pure(y):
        return Tree(classifier=y[0])

    elif len(X.T) == 0:
        #All datapoints have identical feature values return a leaf with the most common label
        return Tree(classifier=most_common(y))
        
    else:
        index = maxgain(X, y, impurity_func)
        tree = Tree(branch="ROOT")

        for feature_value, [X_spl, y_spl] in split(X, y, index).items():

            child = make_tree(X_spl, y_spl, impurity_func)
            child.branch = feature_value
            tree.append_child(child) 
        return tree

def split(X, y, index):
    """
    Split X and y on the values that co occur with the value in the selected column of X

    Args:
        X (numpy.ndarray): A matrix of feature values.
        y (numpy.ndarray): A list of classifiers.
        index (int): index of X with the best feature.
    Returns:
        dictionary: Where the split feature value maps to a new X and y.
    """

    spl = {}
    for i in range(len(y)):
        if X[i][index] in spl:
            x = np.concatenate((X[i][:index], X[i][index+1:]))
            
            spl[X[i][index]][0] = np.vstack([spl[X[i][index]][0], x])
            spl[X[i][index]][1].append(y[i])
            
        else:
            x= np.concatenate((X[i][:index], X[i][index+1:]))
            spl[X[i][index]] = [x, [y[i]]]
    return spl
    
def maxgain(X, y, impurity_func):
    """
    Calculates the information gain for each feature and the classifier.

    Args:
        X (list): Matrix where each column has the values of a feature.
        y (list): List of values of a classifier.
        impurity_func (function): For calculating the gain (e.g entropy)
    Returns:
        int: The index of the column of X that gives the largest information gain.
    """
    probabilities = [occ / len(y) for occ in occurrences(y).values()]
    y_impurity = impurity_func(probabilities)

    gains = []
    for x in X.T:
        gain = y_impurity
        for occ_dict in co_occurrences(x, y).values():
            n = sum(occ_dict.values())
            probabilities = [occ / n for occ in occ_dict.values()]
            gain -= (n / len(y)) * impurity_func(probabilities)

        gains.append(gain)        
    index = np.argmax(gains)
    return index

def is_pure(values):
    """
    Args:
        values (list): A list of some object
    Returns:
        Whether values only has identical objects
    """
    return len(values) <= 1 or list(values).count(values[0]) == len(values)

def most_common(values):
    """
    Args:
        values (list): A list of some object. 
    Returns:
        The most common object in values.
    """
    occs = occurrences(values)
    return max(occs, key=occs.get)

def occurrences(values):
    """
    Args:
        values (list): A list of some object.
    Returns:
        dictionary: With each key maps to the number of occurrences of that key in the given list.
    """
    occs = {}
    for value in values:
        occs[value] = occs.get(value, 0) + 1
    return occs

def co_occurrences(values1, values2):
    """
    Count how many times each value in xs occurs with ys (at the same position)

    Args:
        values1 (list): A list of some object
        values2 (list): A list of some object
    Returns:
        dictionary: Where each value maps to a dictionary with the counts of co occurrence.
    Raises:
        AssertionError: If length of argument lists are unequal.
    """
    assert len(values1) == len(values2), "Lists must be same length"

    occs = {}
    for x, y in list(zip(values1, values2)):
        if x in occs:
            if y in occs[x]:
                occs[x][y] += 1
            else:
                occs[x][y] = 1
        else:
            occs[x] = {y : 1}
    return occs

def entropy(probabilities):
    """
    Args: 
        probabilities (list): A list of probabilities.
    Returns:
        float: The entropy.
    """
    sum_ = 0
    for p in probabilities:
        sum_ += -p * np.log2(p) if p != 0 else 0
    return sum_ 

def gini(probabilities):
    """
    Args: 
        probabilities (list): A list of probabilities.
    Returns:
        float: The gini index
    """
    sum_ = 0
    for p in probabilities:
        sum_ += p * (1 - p)
    return sum_

def predict(x, tree):
    """
    Args:
        x (list): List of feature values
        tree (Tree): A decision tree
    Returns:
        A prediction, None if x contains feature values not present in the tree.
    """
    if tree.is_leaf():
        return tree.classifier
    else:

        for node in tree.children:
            if node.branch in x:
                x = x.tolist()
                x.remove(node.branch)
                return predict(np.array(x), node)
        return None

def prune(X, y, tree):
    """
        Post pruning the tree. Traverse tree until the leafs recursively. 
        Sum the errors of the subtree and remove if they don't improve the tree.
        Args:
            X (numpy.ndarray): A pruning dataset.
            y (list): Pruning classifiers.
    """
    if tree.is_leaf():
        return
    
    for child in tree.children:
        prune(child)

    error = 0
    for child in tree.children:
        for i in range(len(y)):
            if predict(X[i], tree.children) != y[i]:
                error += 1
            
    gain = error # - noe
    if gain > 0:
        tree.clear_children()
        #tree.classifier =  most common label av kids
            

def first_occurence(values, z):
    for i in range(len(values)):
        if values[i] == z:
            return i
    return -1